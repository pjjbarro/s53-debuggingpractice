// Question: What would be the output of the loop?

for (let i = 1; i < 5; i++) {
  console.log(i * i);

  //1x1 i - 1
  //2x2 i - 4

}

//Answer: 1 4 9 16

// Question: What would be the problem in the code snippet?


let students = ['John', 'Paul', 'George', 'Ringo'];

console.log('Here are the graduating students:');

for (let count = 0; count <= students.length; count++) {
  console.log(students[count]);
}

//students[0] - John
//students[1] - Paul
//students[2] - George
//students[3] - Ringo
//students[4] //undefined.

//Answer: There will be an extra *undefined* in the console

// Question: What would be the console output of the function?

function checkGift(day) {
  let gifts = [
    'partridge in a pear tree',
    'turtle doves',
    'french hens',
    'golden rings'
  ];

  if (day > 0 && day < 4) {
    return `I was given ${day} ${gifts[day-1]}`;
  } else {
    return `No gifts were given`;
  }
}

//view the the returned output of the function with console.log()
console.log(checkGift(3));

//No visible output in the console. The function returns a string as a value but does not display or log in the console.

// What would be the problem in the code snippet?

let items = [
  {
    id: 1,
    name: 'Banana',
    description: 'A yellow fruit',
    price: 15.00,
    category: 2
  },
  {
    id: 2,
    name: 'Pork Cutlet',
    description: 'Japanese kurobuta',
    price: 15.00,
    category: 1
  }, 
  {
    id: 1,
    name: 'Sweet Potato',
    description: 'Best when roasted',
    price: 15.00,
    category: 3
  }
];

for (let i = 0; i < items.length; i++) {
  console.log(`
    Name: ${items[i].name}
    Description: ${items[i].description}
    Price: ${items[i].price}
  `);
}

//There will be no problem. There was no error in the array or the elements inside it nor were there any errors in the condition for the loop, so not like the first question, it will not iterate undefined.

// Question: What would be the output?

for (let row = 1; row < 3; row++) {
  for (let col = 1; col <= row; col++) {
    //first iteration: col = 1 row = 1
    //second iteration: col = 2 row = 1
    console.log(`Current row: ${row}, Current col: ${col}`);
  }
}

/*
   Current row: 1, Current col: 1
   Current row: 2, Current col: 1
   Current row: 2, Current col: 2 
*/



// Question: What would be the problem in the code snippet?

/*function checkLeapYear(year) {
  if (year % 4 = 0) {
    if (year % 100 = 0) {
      if (year % 400 = 0) {
        console.log('Leap year');
      } else {
        console.log('Not a leap year');
      }
    } else {
      console.log('Leap year');
    }
  } else {
    console.log('Not a leap year');
  }
}

checkLeapYear(1999);
*/
//There will be a syntax error. = is an assignment operator and not a comparison operator(== or ===)

// Question: Given the array below, how can the last student's English grade be displayed?

let records = [
  {
    id: 1,
    name: 'Brandon',
    subjects: [
      { name: 'English', grade: 98 },
      { name: 'Math', grade: 66 },
      { name: 'Science', grade: 87 }
    ]
  },
  {
    id: 2,
    name: 'Jobert',
    subjects: [
      { name: 'English', grade: 87 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 74 }
    ]
  },
  {
    id: 3,
    name: 'Junson',
    subjects: [
      { name: 'English', grade: 60 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 87 }
    ]
  }
];

console.log(records[2].subjects[0].grade)

// Question: What would be the problem in the code snippet?

function checkDivisibility(dividend, divisor) {

  //Mini-Activity:
  //Add a validation to this function which will show an alert if the user inputs 0 as a divisor

    if (dividend % divisor == 0) {
      console.log(`${dividend} is divisible by ${divisor}`);
    } else {
      console.log(dividend / divisor)//infinity
      console.log(`${dividend} is not divisible by ${divisor}`);
    }

 

}

checkDivisibility(100, 0);

//Division by 0 is not possible
